%bcond_with pcp
Name:           cockpit
Version:        178
Release:        7
Summary:        A easy-to-use, integrated, glanceable, and open web-based interface for Linux servers
License:        LGPLv2+
URL:            https://cockpit-project.org/
Source0:        https://github.com/cockpit-project/cockpit/releases/download/%{version}/cockpit-%{version}.tar.xz

Patch6000:      CVE-2019-3804.patch

BuildRequires:  pkgconfig(gio-unix-2.0) pkgconfig(json-glib-1.0) pkgconfig(polkit-agent-1) >= 0.105 pam-devel
BuildRequires:  autoconf automake python3 intltool libssh-devel >= 0.7.1 openssl-devel zlib-devel krb5-devel
BuildRequires:  libxslt-devel docbook-style-xsl glib-networking sed glib2-devel >= 2.37.4
BuildRequires:  systemd-devel krb5-server gdb xmlto

%if %{with pcp}
BuildRequires: pcp-libs-devel
%endif

Requires:       glib-networking shadow-utils grep libpwquality coreutils NetworkManager kexec-tools openssl glib2 >= 2.37.4
Requires:       python3 python3-dbus systemd udisks2 >= 2.6 libvirt libvirt-client PackageKit

Provides:       %{name}-networkmanager %{name}-selinux %{name}-sosreport %{name}-dashboard = %{version}-%{release}
Provides:       %{name}-machines = %{version}-%{release} %{name}-machines-ovirt = %{version}-%{release} %{name}-shell %{name}-systemd
Provides:       %{name}-bridge = %{version}-%{release} %{name}-packagekit = %{version}-%{release} %{name}-storaged = %{version}-%{release}
Provides:       %{name}-system = %{version}-%{release} %{name}-ws = %{version}-%{release} %{name}-ssh %{name}-realmd
Provides:       %{name}-tuned %{name}-users %{name}-kdump
Provides:       bundled(js-jquery) = 3.3.1 bundled(js-moment) = 2.22.2 bundled(nodejs-flot) = 0.8.3 bundled(xstatic-patternfly-common) = 3.35.1
Provides:       bundled(nodejs-promise) = 8.0.2 bundled(nodejs-requirejs) = 2.1.22 bundled(xstatic-bootstrap-datepicker-common) = 1.8.0

Obsoletes:      %{name}-networkmanager %{name}-selinux %{name}-sosreport %{name}-dashboard < %{version}-%{release}
Obsoletes:      %{name}-machines < %{version}-%{release} %{name}-machines-ovirt < %{version}-%{release} %{name}-shell %{name}-systemd
Obsoletes:      %{name}-bridge < %{version}-%{release} %{name}-packagekit < %{version}-%{release} %{name}-storaged < %{version}-%{release}
Obsoletes:      %{name}-system < %{version}-%{release} %{name}-ws < %{version}-%{release} %{name}-ssh %{name}-realmd
Obsoletes:      %{name}-tuned %{name}-users %{name}-kdump

Conflicts:      %{name}-dashboard < 170.x %{name}-ws < 135 firewalld < 0.6.0-1

Recommends:     polkit NetworkManager-team setroubleshoot-server >= 3.3.3 sscg >= 2.3 system-logos
Recommends:     udisks2-lvm2 >= 2.6 udisks2-iscsi >= 2.6 device-mapper-multipath clevis-luks virt-install

%description
Cockpit makes GNU/Linux discoverable. See Linux server in a web browser and perform system tasks with a mouse.
It’s easy to start containers, administer storage, configure networks, and inspect logs with this package.

%if %{with pcp}
%package pcp
Summary: Cockpit PCP integration
Requires: cockpit-bridge >= 134.x
Requires: pcp

%description pcp
Cockpit support for reading PCP metrics and loading PCP archives.
%endif

%package devel
Summary:        Test suite for %{name}
Requires:       %{name}-bridge >= 138 %{name}-system >= 138 openssh-clients
Provides:       %{name}-test-assets = %{version}-%{release}
Provides:       %{name}-tests = %{version}-%{release}
Obsoletes:      %{name}-test-assets < 132
Obsoletes:      %{name}-tests < %{version}-%{release}

%description devel
This package contains some test files for testing the %{name}.
It is not necessary for using %{name}.

%package help
Summary:        Help package for %{name}
BuildArch:      noarch
Requires:       %{name} = %{version}-%{release}
Provides:       %{name}-doc = %{version}-%{release}
Obsoletes:      %{name}-doc < %{version}-%{release}

%description help
This package helps you to deploy %{name} and contains some
man help files.

%prep
%autosetup -n %{name}-%{version} -p1

%build
%configure --disable-silent-rules --with-cockpit-user=cockpit-ws --with-selinux-config-type=etc_t\
  --with-appstream-data-packages='[ "appstream-data" ]' --with-nfs-client-package='"nfs-utils"' --with-vdo-package='"vdo"' \
%if %{without pcp}
  --disable-pcp
%endif

%make_build

%check
%if %{?_with_check:1}%{!?_with_check:0}
%make_build check
%endif

%install
%make_install
make install-tests DESTDIR=%{buildroot}

install -Dpm644 tools/cockpit.pam %{buildroot}%{_sysconfdir}/pam.d/cockpit

echo '{ "linguas": null }' > %{buildroot}%{_datadir}/cockpit/shell/override.json

%find_lang %{name}

tar -C %{buildroot}/usr/src/debug%{_datadir}/%{name}/playground -cf - . | tar -C %{buildroot}%{_datadir}/%{name}/playground -xf -

%pre
getent group cockpit-ws >/dev/null || groupadd -r cockpit-ws
getent passwd cockpit-ws >/dev/null || useradd -r -g cockpit-ws -d / -s /sbin/nologin -c "User for cockpit-ws" cockpit-ws

%post
%systemd_post cockpit.socket
test -f %{_bindir}/firewall-cmd && firewall-cmd --reload --quiet || true
# cannot use systemctl because it might be out of sync with reality

%if %{with pcp}
%post pcp
/usr/share/pcp/lib/pmlogger condrestart
%endif

%preun
%systemd_preun cockpit.socket

%postun
%systemd_postun_with_restart cockpit.socket
%systemd_postun_with_restart cockpit.service

%files -f %{name}.lang
%if %{without pcp}
%exclude %{_datadir}/cockpit/pcp/*
%endif
%exclude %{_prefix}/lib/firewalld/services/cockpit.xml
%exclude %{_datadir}/%{name}/{subscriptions,docker,kubernetes}
%exclude %{_datadir}/pixmaps/cockpit-sosreport.png
%exclude %{_libexecdir}/{cockpit-kube-auth,cockpit-kube-launch,cockpit-stub}
%exclude %{_metainfodir}/{org.cockpit-project.cockpit-sosreport.metainfo.xml,org.cockpit-project.cockpit-kdump.metainfo.xml,org.cockpit-project.cockpit-selinux.metainfo.xml}
%doc AUTHORS COPYING README.md
%config(noreplace) %{_sysconfdir}/cockpit/ws-certs.d
%config(noreplace) %{_sysconfdir}/pam.d/cockpit
%config %{_sysconfdir}/issue.d/cockpit.issue
%config %{_sysconfdir}/motd.d/cockpit
%{_datadir}/metainfo/cockpit.appdata.xml
%{_datadir}/applications/cockpit.desktop
%{_datadir}/pixmaps/cockpit.png
%{_datadir}/%{name}/motd/{update-motd,inactive.motd}
%{_datadir}/%{name}/{static,branding}
%{_datadir}/%{name}/{base1,ssh,dashboard,realmd,tuned,shell,systemd,users,kdump,sosreport,storaged,networkmanager,packagekit,apps,machines,ovirt,selinux}/*
%{_unitdir}/{cockpit.service,cockpit-motd.service,cockpit.socket}
%{_sysconfdir}/%{name}/machines.d
%{_prefix}/lib/tmpfiles.d/cockpit-tempfiles.conf
%attr(0775,-,wheel) %{_localstatedir}/lib/cockpit
%{_libdir}/security/pam_ssh_add.so
%{_libexecdir}/{cockpit-askpass,cockpit-ws,cockpit-ssh}
%attr(4750,root,cockpit-ws) %{_libexecdir}/cockpit-session
%{_sbindir}/remotectl
%{_bindir}/cockpit-bridge

%if %{with pcp}
%files pcp
%{_datadir}/cockpit/pcp/*
%{_libexecdir}/cockpit-pcp
%{_localstatedir}/lib/pcp/config/pmlogconf/tools/cockpit
%endif

%files devel
%config(noreplace) %{_sysconfdir}/cockpit/cockpit.conf
%{_datadir}/cockpit/playground
%{_prefix}/lib/cockpit-test-assets

%files help
%{_docdir}/cockpit
%exclude %{_docdir}/cockpit/{AUTHORS,COPYING,README.md}
%doc %{_mandir}/man1/{cockpit.1.gz,cockpit-bridge.1.gz}
%doc %{_mandir}/man5/cockpit.conf.5.gz
%doc %{_mandir}/man8/{cockpit-ws.8.gz,remotectl.8.gz,pam_ssh_add.8.gz}

%changelog
* Sun Jan 19 2020 openEuler Buildteam <buildteam@openeuler.org> - 178-7
- Type:bugfix
- Id:NA
- SUG:NA
- DESC:bugfix about the confict about cockpit-help and cockpit

* Sun Jan 12 2020 zhangrui <zhangrui182@huawei.com> - 178-6
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:fix CVE-2019-3804

* Mon Oct 28 2019 caomeng <caomeng5@huawei.com> - 178-5
- Type:NA
- ID:NA
- SUG:NA
- DESC:add bcondwith pcp

* Wed Sep 25 2019 huzhiyu<huzhiyu1@huawei.com> - 178-4
- Type:bugfix
- Id:NA
- SUG:NA
- DESC:fix firewalld conflicts

* Tue Sep 24 2019 openEuler Buildteam <buildteam@openeuler.org> - 178-3
- Type:bugfix
- Id:NA
- SUG:NA
- DESC:add pcp packages for cockpit

* Sat Sep 21 2019 huzhiyu<huzhiyu1@huawei.com> - 178-2
- Package init
